package controller;

public enum ShapeTool {

    LINE, RECTANGLE, ELLIPSE, ROUND_RECTANGLE
}
